package eshop.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static eshop.pageObjects.Homepage.cats;

public class SelectACategory implements Task {
    String category;

    public SelectACategory(String category) {
        this.category = category;
    }

    public static SelectACategory called(String category) {
        return Instrumented.instanceOf(SelectACategory.class).withProperties(category);
    }

    @Override
    @Step("{1} Selects category")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(cats.get(category), WebElementStateMatchers.isVisible()),
                Click.on(cats.get(category))
        );
    }
}
