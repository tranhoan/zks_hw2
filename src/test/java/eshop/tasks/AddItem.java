package eshop.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static eshop.pageObjects.CategoryPage.findItem;
import static eshop.pageObjects.CategoryPage.FIRST_ITEM;
import static eshop.pageObjects.ItemPage.ADD_TO_CART_BUTTON;
import static eshop.pageObjects.ItemPage.ORDER_BUTTON;
import static eshop.pageObjects.ShoppingCartPage.MOST_RECENT_ITEM;

public class AddItem implements Task {
    int itemIndex;

    public AddItem(int itemIndex) {
        this.itemIndex = itemIndex;
    }

    public static AddItem called(int index) {
        return Instrumented.instanceOf(AddItem.class).withProperties(index);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        Target item = findItem(itemIndex);
        actor.attemptsTo(
                WaitUntil.the(item, WebElementStateMatchers.isVisible()),
                Click.on(item)
        );
        actor.attemptsTo(
                WaitUntil.the(ADD_TO_CART_BUTTON, WebElementStateMatchers.isVisible()),
                Click.on(ADD_TO_CART_BUTTON)
        );
        actor.attemptsTo(
                WaitUntil.the(ORDER_BUTTON, WebElementStateMatchers.isVisible()),
                Click.on(ORDER_BUTTON)
        );
        actor.attemptsTo(
                WaitUntil.the(MOST_RECENT_ITEM, WebElementStateMatchers.isVisible())
        );
    }
}
