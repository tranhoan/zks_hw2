package eshop.tasks;

import eshop.pageObjects.Homepage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartWith implements Task {

    Homepage applicationHomePage;

    public static StartWith anEmptyCart() {
        return instrumented(StartWith.class);
    }

    @Override
    @Step("{0} Loads main page")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage)
        );
    }
}