package eshop;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import eshop.questions.CategoryQuestion;
import eshop.tasks.AddItem;
import eshop.tasks.SelectACategory;
import eshop.tasks.StartWith;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.WhenPageOpens;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static eshop.questions.CategoryQuestion.theDisplayedCategory;
import static eshop.questions.ShoppingCartQuestion.theDisplayedItem;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

@RunWith(SerenityRunner.class)
public class EshopTests {

    Actor james = Actor.named("James");
    private WebDriver theBrowser;

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_select_a_category() {
        String cat = "T-shirts";
        givenThat(james).wasAbleTo(StartWith.anEmptyCart());
        when(james).attemptsTo(SelectACategory.called(cat));
        then(james).should(seeThat(theDisplayedCategory(), equalTo(cat.toUpperCase())));
    }

    @Test
    public void should_be_able_to_add_the_first_item_to_the_cart() {
        String cat = "T-shirts";
        String item = "Tričko - krátké rukávy";
        givenThat(james).wasAbleTo(StartWith.anEmptyCart());
        when(james).attemptsTo(SelectACategory.called(cat), AddItem.called(1));
        then(james).should(seeThat(theDisplayedItem(), equalTo(item)));
    }



    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
