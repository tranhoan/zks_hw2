package eshop.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

public class CategoryPage extends PageObject {
    public static final Target CATEGORY_TITLE = Target
            .the("Category title")
            .locatedBy(".cat-name");

    public static final Target  findItem(int index) {
        String selector = "ul.product_list li:nth-child("+index+") div.product-container";
        return Target
            .the("Item")
            .locatedBy(selector);
    }

    public static final Target FIRST_ITEM = Target
            .the("Item")
            .locatedBy("ul.product_list li:nth-child(1) div.product-container");
}