package eshop.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

import java.util.HashMap;
import java.util.Map;

@DefaultUrl("https://e-shop.webowky.cz/")
public class Homepage extends PageObject {
    public static Target WOMEN_LINK = Target.the("Women link").located(By.cssSelector(".sf-menu>li>a[title='Women']"));
    public static Target DRESSES_LINK = Target.the("Dresses link").located(By.cssSelector(".sf-menu>li>a[title='Dresses']"));
    public static Target TSHIRTS_LINK = Target.the("T-shirts link").located(By.cssSelector(".sf-menu>li>a[title='T-shirts']"));

    public static Map<String, Target> cats = new HashMap<String, Target>() {{
        put("Women", WOMEN_LINK);
        put("Dresses", DRESSES_LINK);
        put("T-shirts", TSHIRTS_LINK);

    }};
}