package eshop.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ShoppingCartPage extends PageObject {
    public static Target MOST_RECENT_ITEM = Target.the("Most recent item").located(By.cssSelector(".last_item .cart_description .s_title_block"));
}
