package eshop.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ItemPage extends PageObject {
    public static Target ADD_TO_CART_BUTTON = Target.the("Add to cart button").located(By.cssSelector("#add_to_cart button"));
    public static Target ORDER_BUTTON = Target.the("Order button").located(By.cssSelector("a[title='Objednat']"));
}
