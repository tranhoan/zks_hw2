package eshop.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static eshop.pageObjects.CategoryPage.CATEGORY_TITLE;
import static eshop.pageObjects.ShoppingCartPage.MOST_RECENT_ITEM;

public class ShoppingCartQuestion implements Question<String> {
    public static Question<String> theDisplayedItem() {
        return new ShoppingCartQuestion();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(MOST_RECENT_ITEM).viewedBy(actor).asString().trim();
    }
}
