package eshop.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


import static eshop.pageObjects.CategoryPage.CATEGORY_TITLE;

public class CategoryQuestion implements Question<String> {

    public static Question<String> theDisplayedCategory() {
        return new CategoryQuestion();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(CATEGORY_TITLE).viewedBy(actor).asString().trim();
    }
}