package todomvc;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import todomvc.tasks.AddATodoItem;
import todomvc.tasks.StartWith;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItem;
import static todomvc.questions.TodoItemsListQuestion.theDisplayedItems;

@RunWith(SerenityRunner.class)
public class TodoMvcTests {

    Actor james = Actor.named("James");

    private WebDriver theBrowser;

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_add_the_first_todo_item() {
        String todoName = "Learn Selenium";

        givenThat(james).wasAbleTo(StartWith.anEmptyTodoList());
        when(james).attemptsTo(AddATodoItem.called(todoName));
        then(james).should(seeThat(theDisplayedItems(), hasItem(todoName)));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
