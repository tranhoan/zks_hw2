package todomvc.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static todomvc.pageObjects.ApplicationHomePage.NEW_TODO_FIELD;

public class AddATodoItem implements Task {

    String thingToDo;

    public AddATodoItem(String thingToDo) {
        this.thingToDo = thingToDo;
    }

    public static AddATodoItem called(String thingToDo) {
        return Instrumented.instanceOf(AddATodoItem.class).withProperties(thingToDo);
    }

    @Step("{0} adds a todo item called #thingToDo")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Enter.theValue(thingToDo).into(NEW_TODO_FIELD),
            Hit.the(Keys.RETURN).keyIn(NEW_TODO_FIELD)
        );
    }
}