package todomvc.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://todomvc.com/examples/react/")
public class ApplicationHomePage extends PageObject {
    public static final Target NEW_TODO_FIELD = Target
        .the("New Todo Field")
        .locatedBy(".new-todo");

    public static final Target LIST_ITEMS = Target
        .the("List of todo items")
        .locatedBy("ul.todo-list li");
}