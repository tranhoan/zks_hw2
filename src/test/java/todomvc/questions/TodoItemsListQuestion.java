package todomvc.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static todomvc.pageObjects.ApplicationHomePage.LIST_ITEMS;

public class TodoItemsListQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedItems() {
        return new TodoItemsListQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        // ul.todo-list li
        return Text.of(LIST_ITEMS).viewedBy(actor).asList();
    }
}